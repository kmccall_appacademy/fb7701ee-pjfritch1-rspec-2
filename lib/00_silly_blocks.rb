def reverser(&prc)
  sentence = yield
  words = sentence.split
  words.map {|word| word.reverse}.join(" ")
end

def adder(plus = 1, &prc)
  number = yield
  number + plus
end

def repeater(times = 1, &prc)
  times.times do
    prc.call
  end
end
