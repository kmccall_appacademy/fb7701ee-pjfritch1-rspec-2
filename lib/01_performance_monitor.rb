def measure(times = 1, &prc)
  runtimes = []
  times.times do
    start_time = Time.now
    yield
    end_time = Time.now
    runtimes << end_time - start_time
  end

  runtimes.reduce(:+) / runtimes.length
end
